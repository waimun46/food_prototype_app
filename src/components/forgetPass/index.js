import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Form, Item, Input, Label, Text, Button } from 'native-base';


class ForgotPass extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={styles.container}>

        <Form>
          <Item stackedLabel last>
            <Label>Phone Number</Label>
            <Input placeholder='Fill in your phone number' placeholderTextColor='#ccc' />
          </Item>
        </Form>
        {/* <View style={{ padding: 20 }}>
          <Text note style={{ textAlign: 'left' }}>
            Please enter your phone number in field above, once completed, you will receive a phone SMS containing a confirmation code shortly.
            This confirmation code will be used for the final step in resetting your phone number.
          </Text>
        </View> */}
        <View style={{ padding: 20, marginTop: 10 }}>
          <Button onPress={() => navigateActions('login')} block style={{ backgroundColor: '#FF6347', height: 50 }}>
            <Text>SUBMIT</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },

})

export default ForgotPass;

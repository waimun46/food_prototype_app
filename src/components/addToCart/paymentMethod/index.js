import React, { Component } from 'react';
import { View , Image, StyleSheet} from 'react-native';
import { ListItem, CheckBox, Text, Body, Right } from 'native-base';
import mylogo from '../../../assest/maybank.jpg'
import visaLogo from '../../../assest/c3.png'



class PaymentMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      maybank: false,
      creadit: false,
    };
  }

  toggleMaybank() {
    this.setState({ maybank: true, creadit: false })
  }

  toggleCredit() {
    this.setState({ creadit: true, maybank: false })
  }



  render() {
    const { maybank, creadit } = this.state;
    const navigateActions = this.props.navigation.navigate;

    console.log('maybank', maybank);
    console.log('creadit', creadit)
    return (
      <View style={styles.container}>
        <ListItem >
          <CheckBox checked={maybank} onPress={() => this.toggleMaybank()} />
          <Body style={{borderBottomWidth: 0}}>
            <Text style={{ paddingLeft: 10 }}>MayBank</Text>
          </Body>
          <Right style={{borderBottomWidth: 0}}>
            <Image source={mylogo} style={{width: 50, height:35,  resizeMode: 'contain',  textAlign: 'right'}} />
          </Right>
        </ListItem>
        <ListItem>
          <CheckBox checked={creadit} onPress={() => this.toggleCredit()} />
          <Body style={{borderBottomWidth: 0}}>
            <Text style={{ paddingLeft: 10 }}>VISA / Credit </Text>
          </Body>
          <Right style={{borderBottomWidth: 0}}>
            <Image source={visaLogo} style={{width: 80, height:35, resizeMode: 'contain', textAlign: 'right'}} />
          </Right>
        </ListItem>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },

})

export default PaymentMethod;

import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Linking, Platform } from 'react-native';
import { QRscanner } from 'react-native-qr-scanner';


class ScanPage extends Component {
  static navigationOptions = {
    title: 'Scan ORCode',
  };
  constructor(props) {
    super(props);
    this.state = {
      flashMode: false,
      zoom: 0.2,
    };
  }

  /************************************* onSuccess Scan *************************************/
  onSuccess = (e) => {
    Linking
      .openURL(e.data)
      .catch(err => console.error('An Error Scan', err));
  }

  /************************************* Flashlight Phone *************************************/
  bottomView = () => {
    return (
      <View style={styles.bottomViewWarp}>
        <TouchableOpacity
          style={styles.bottomTouchableOpacity}
          onPress={() => this.setState({ flashMode: !this.state.flashMode })}
        >
          <View style={styles.bottomTextWaro}>
            <Text style={StyleSheet.bottomText}>ON/OFF</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const { zoom, flashMode } = this.state;
    return (
      <View style={styles.container}>
        <QRscanner
          onRead={this.onSuccess}
          renderBottomView={this.bottomView}
          flashMode={flashMode}
          zoom={zoom}
          finderY={0}
          hintText=" "
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...Platform.select({
      ios: { backgroundColor: 'black' },
      android: { backgroundColor: 'black' }
    })
  },
  bottomViewWarp: { flex: 1, flexDirection: 'row', backgroundColor: '#0000004D' },
  bottomTouchableOpacity: { flex: 1, alignItems: 'center', justifyContent: 'center' },
  bottomTextWaro: { backgroundColor: '#ffffff52', padding: 10, borderRadius: 20, borderWidth: 0 },
  bottomText: { color: '#fff', }
});

export default ScanPage;

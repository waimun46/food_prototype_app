import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { Text, ListItem, Left, Body, Right, Button, Thumbnail, List } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';


const data = [
  { item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 3.6 },
  { item: 'PRODUCT 2', name: 'This is my products.', price: '50.00', img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 4.6 },
  { item: 'PRODUCT 3', name: 'This is my products.', price: '30.00', img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', star: 3.8 },

]

class WishlistPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _renderWishlist = ({ item, index }) => {
    return (
      <List style={{ backgroundColor: '#fff', marginBottom: 10 }}>
        <ListItem avatar style={{ paddingBottom: 15, marginLeft: 10 }}>
          <Left style={{ borderBottomWidth: 0 }}>
            <Thumbnail square source={{ uri: item.img }} style={{ width: 80, height: 80 }} />
          </Left>
          <Body style={{ borderBottomWidth: 0 }}>
            <Text>{item.item}</Text>
            <Text note style={{ marginTop: 5 }}>{item.name}</Text>
            {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
              <ANT name="star" color="#FFD700" size={15} style={{ paddingRight: 5 }} />
              <Text note style={{ color: '#000' }}>{item.star}</Text>
            </View> */}
            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
              <Text style={{ fontWeight: 'bold' }}>RM {item.price}</Text>

            </View>

          </Body>
          <Right style={{ borderBottomWidth: 0 }}>
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity style={{ paddingBottom: 10 }}>
              <ANT name="delete" color={"#ccc"} size={20} style={{ textAlign: 'right', }} />
            </TouchableOpacity>
            {/* <TouchableOpacity style={{ paddingBottom: 10 }}>
              <Text style={{ fontWeight: 'bold' }}>RM {item.price}</Text>
            </TouchableOpacity> */}
            </View>
          </Right>
        </ListItem>
      </List>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.title}>
            <Text note >Watchlist</Text>
          </View>

          <FlatList
            data={data}
            renderItem={this._renderWishlist}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: 50 }}
          />

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  title: { marginTop: 15, paddingBottom: 10, paddingLeft: 10, paddingRight: 10 },


})

export default WishlistPage;

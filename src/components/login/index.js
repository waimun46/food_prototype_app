import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { Form, Item, Input, Label, Button, Text } from 'native-base';
import MATI from 'react-native-vector-icons/MaterialCommunityIcons';
import FA from 'react-native-vector-icons/FontAwesome';


class Loginpage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: null,
      password: null,
    };
  }

  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
    if (field === 'password') { this.setState({ password: text }) }
  }


  render() {
    const { phone, password } = this.state;
    const navigateActions = this.props.navigation.navigate;

    console.log('phone', phone)
    console.log('password', password)

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ backgroundColor: '#f48120', height: 200, justifyContent: 'center', alignItems: 'center', paddingBottom: 30 }}>
            <MATI name="food-apple" color={'#fff'} size={80} />
            <Text note style={{ color: '#fff' }}>Your Logo</Text>
          </View>
          <View style={{ padding: 20, paddingLeft: 10 }}>
            <Form style={{ paddingRight: 0 }} >
              <Item stackedLabel >
                <Label >Phone Number</Label>
                <Input
                  value={phone}
                  placeholder="EXP: 0123456789"
                  placeholderTextColor="#ccc"
                  onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
                  autoCorrect={false}
                  autoCapitalize="none"
                  //secureTextEntry={true}
                  keyboardType={'numeric'}
                  style={{ fontSize: 15 }}
                />
              </Item>
              <Item stackedLabel >
                <Label>Password</Label>
                <Input
                  value={password}
                  placeholder="Password"
                  placeholderTextColor="#ccc"
                  onChangeText={(text) => this.onChangeTextInput(text, 'password')}
                  autoCorrect={false}
                  autoCapitalize="none"
                  secureTextEntry={true}
                  style={{ fontSize: 15 }}
                />
              </Item>
            </Form>
          </View>

          <View style={{ padding: 20 }}>
            <Button onPress={() => navigateActions('home')} block style={{ backgroundColor: '#f48120', height: 50 }}>
              <Text>LOGIN</Text>
            </Button>
            <Button full dark style={styles.btnwarpSms} onPress={() => navigateActions('sms')}>
              <FA name='envelope' style={{ color: '#fff', paddingRight: 20 }} size={25} />
              <Text style={styles.btntext}>Login With SMS</Text>
            </Button>
          </View>

          <View style={{ paddingRight: 20, paddingBottom: 10, paddingTop: 10 }}>
            <TouchableOpacity onPress={() => navigateActions('forgotPass')}>
              <Text note style={{ textAlign: 'right', color: '#00BFFF' }}>Forget Password ?</Text>
            </TouchableOpacity>
          </View>

          <View style={{ alignItems: 'center', marginTop: 20, }}>
            <View style={{ flexDirection: 'row', }}>
              <Text note >Don't have a account ? </Text>
              <TouchableOpacity onPress={() => navigateActions('register')}>
                <Text note style={{ color: '#f48120', fontWeight: 'bold' }}>Sign Up</Text>
              </TouchableOpacity>
            </View>

          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
  btnwarpSms: {height: 50,marginTop: 20, borderRadius: 5 },
  btntext: { color: '#fff', },
})


export default Loginpage;

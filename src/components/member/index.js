import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TextInput, TouchableOpacity, Image, } from 'react-native';
import { Text, Button, Item, Input, Picker } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import { showMessage, hideMessage } from "react-native-flash-message";
import Countdown from 'react-countdown-now';
import FOT from 'react-native-vector-icons/Fontisto';
import cardimg from '../../assest/card.jpg';
import ANT from 'react-native-vector-icons/AntDesign';

const stateData = [
  { state: 'Kuala Lumpur', }, { state: 'Negeri Sembilan', }, { state: 'Selangor', }, { state: 'Kuantan', }
  , { state: 'Perak', }, { state: 'Johor', }
]


class JoinMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      first_name: '',
      last_name: '',
      email: '',
      address_1: '',
      address_2: '',
      postcode: '',
      state: '',
      taccode: '',
      referral_id: '',
      btnTac: false,
      restartTime: Date.now() + 180000,
      selected: ""
    };
  }

  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }



  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
    if (field === 'email') { this.setState({ email: text }) }
    if (field === 'first_name') { this.setState({ first_name: text }) }
    if (field === 'last_name') { this.setState({ last_name: text }) }
    if (field === 'address_1') { this.setState({ address_1: text }) }
    if (field === 'address_2') { this.setState({ address_2: text }) }
    if (field === 'postcode') { this.setState({ postcode: text }) }
    if (field === 'state') { this.setState({ state: text }) }
    if (field === 'taccode') { this.setState({ taccode: text }) }
    if (field === 'referral_id') { this.setState({ referral_id: text }) }
  }

  onBtnTac() {
    this.setState({
      btnTac: !this.state.btnTac,
      restartTime: Date.now() + 180000,
    })
  }

  /****************************************************** resendTacCode ********************************************************/
  resendTacCode() {
    // let path = "forgot_password_recode";
    // let parameters = { MMID: this.state.mmidTokenForget };
    // // let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    this.setState({
      restartTime: Date.now() + 180000,
    })

    let that = this;

    // fetch(ApiData).then((res) => res.json())
    //   .then(function (myJson) {
    //     //console.log(myJson, 'myJson-----------');
    //     if (myJson[0].status === 1) {
    //       Alert.alert('Tac Code already SMS your phone number', '',
    //         [
    //           {
    //             text: 'OK', onPress: () => that.setState({ isSubmit: false, })
    //           },
    //         ],
    //         { cancelable: false },
    //       );
    //     }
    //     else {
    //       Alert.alert(myJson[0].error, '',
    //         [
    //           {
    //             text: 'OK', onPress: () => that.setState({ isSubmit: false, })
    //           },
    //         ],
    //         { cancelable: false },
    //       );
    //     }
    //   })
  }

  /****************************************** renderer resend tac ********************************************/
  renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      return (
        <TouchableOpacity style={styles.resendBtn} onPress={() => this.resendTacCode()}>
          <Text note style={{ color: '#0095ff', }}>Resend TAC Code</Text>
        </TouchableOpacity>
      );
    }
    else {
      return (
        <View style={styles.timeCount}>
          <Text style={styles.secondText}>{minutes}:{seconds}</Text>
        </View>
      );
    }
  }





  render() {
    const { phone, email, first_name, last_name, address_2, address_1, postcode, state,
      btnTac, taccode, restartTime, referral_id } = this.state;
    const navigateActions = this.props.navigation.navigate;
    return (
      <ScrollView >
      <View style={styles.container}>


          <View style={{ alignItems: 'center', paddingBottom: 50}}>


            <View style={{ width: '70%', height: 200, padding: 20, paddingBottom: 0, marginBottom: 10 }}>
              <Image source={cardimg} style={{ resizeMode: 'contain', width: '100%', height: '100%' }} />
            </View>


            <View style={styles.contentWarp}>

              {/************************** Frist Name ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>First Name</Text>
                <Item >
                  <TextInput
                    value={first_name}
                    placeholder="Fill in your first name"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'first_name')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
              </View>

              {/************************** Last Name ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Last Name</Text>
                <Item >
                  <TextInput
                    value={last_name}
                    placeholder="Fill in your last name"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'last_name')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
              </View>


              {/************************** Email ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Email</Text>
                <Item>
                  <TextInput
                    value={email}
                    placeholder="EXP: abc@mail.com"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'email')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
              </View>

              {/************************** Address ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Address</Text>
                <Item>
                  <TextInput
                    value={address_1}
                    placeholder="Address line 1"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'address_1')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
                <Item style={{ marginTop: 10 }}>
                  <TextInput
                    value={address_2}
                    placeholder="Address line 2"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'address_2')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                  <View style={{ width: '50%', paddingRight: 15 }}>
                    <View>
                      <Text note style={styles.labelsty}>State</Text>
                      <Item >
                        <Picker
                          mode="dropdown"
                          iosHeader="Select State"
                          headerTitleStyle={{ color: '#fff' }}
                          headerStyle={{ backgroundColor: "#f48120" }}
                          headerBackButtonTextStyle={{ color: "#fff" }}
                          headerBackButtonText={<ANT name="left" size={22} />}
                          iosIcon={<ANT name="down" size={10} style={styles.iconsty} />}
                          style={{ width: undefined, height: 45, }}
                          selectedValue={this.state.selected}
                          onValueChange={this.onValueChange.bind(this)}
                          textStyle={{ fontSize: 15, paddingLeft: 0 }}
                          placeholder='Select State'
                          placeholderStyle={{ color: '#ccc' }}
                        >
                          {
                            stateData.map((item, index) => {
                              return (
                                <Picker.Item label={item.state} value={item.state} key={index} />
                              )
                            })
                          }
                        </Picker>
                      </Item>
                    </View>
                  </View>
                  <View style={{ width: '50%', paddingLeft: 15 }}>
                    <View>
                      <Text note style={styles.labelsty}>Postcode</Text>
                      <Item >
                        <TextInput
                          value={postcode}
                          placeholder="EXP: 12345"
                          placeholderTextColor="#ccc"
                          onChangeText={(text) => this.onChangeTextInput(text, 'postcode')}
                          autoCorrect={false}
                          autoCapitalize="none"
                          //secureTextEntry={true}
                          keyboardType={'numeric'}
                          style={styles.inputsty}
                        />
                      </Item>
                    </View>
                  </View>
                </View>
              </View>


              {/************************** Phone Number ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Phone Number</Text>
                <Item>
                  <TextInput
                    value={phone}
                    placeholder="EXP: 0120000000"
                    placeholderTextColor="#ccc"
                    style={[styles.inputsty,{width: '70%'}]}
                    onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
                    autoCorrect={false}
                    autoCapitalize="none"
                    keyboardType={'numeric'}
                  />
                  <TouchableOpacity onPress={() => this.onBtnTac()} style={{ backgroundColor: '#48D1CC', padding: 5 , width: '30%', alignItems: 'center'}}>
                    {/* <FA name="mobile" size={40} color='#ccc' /> */}
                    <Text note style={{ color: '#fff', textAlign: 'center' }}>GET OPT</Text>
                  </TouchableOpacity>
                </Item>
              </View>

              {/************************** Referral ID ****************************/}
              <View style={{ marginBottom: 15 }}>
                <Text note style={styles.labelsty}>Referral ID</Text>
                <Item >
                  <TextInput
                    value={referral_id}
                    placeholder="EXP: 1234567"
                    placeholderTextColor="#ccc"
                    style={styles.inputsty}
                    onChangeText={(text) => this.onChangeTextInput(text, 'referral_id')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  // keyboardType={'numeric'}
                  />
                </Item>
              </View>


              {
                btnTac === false ? null :
                  <View>
                    <Text note style={{ marginTop: 20 }}>
                      We have sent you an SMS with a code to the number above,
                      To complete your phone number verification, please enter the 6-digit activation code.
                  </Text>
                    <Item>
                      <TextInput
                        value={taccode}
                        placeholder="******"
                        placeholderTextColor="#8c8c8c"
                        style={styles.inputstyTac}
                        onChangeText={(text) => this.onChangeTextInput(text, 'taccode')}
                        autoCorrect={false}
                        autoCapitalize="none"
                        keyboardType={'numeric'}
                      />
                    </Item>
                    {/************************** Countdown ****************************/}
                    <View style={{ marginTop: 10 }}>
                      <Countdown
                        date={restartTime}
                        renderer={this.renderer}
                        key={restartTime}
                      />
                    </View>
                  </View>
              }

              {/************************** button submit ****************************/}
              <Button full dark style={styles.btnwarp} onPress={() => navigateActions('profile')}>
                <Text style={styles.btntext}>JOIN</Text>
              </Button>

            </View>
          </View>

      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', },
  inputsty: { marginBottom: 5, paddingLeft: 0, fontSize: 15, height: 40 },
  inputstyTac: {
    height: 50, padding: 10, color: '#000', width: '100%', marginTop: 10,
    marginBottom: 10, textAlign: 'center', letterSpacing: 20
  },
  btnwarp: { marginTop: 30, borderRadius: 5, height: 50, width: '100%', },
  btntext: { color: '#fff', },
  iconWarp: { width: 130, height: 130, borderRadius: 100, backgroundColor: '#f4812026', alignItems: 'center', justifyContent: 'center' },
  iconContainer: { alignItems: 'center', marginTop: 20, marginBottom: 20 },
  contentWarp: { marginTop: 20, width: '85%', },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },
  resendBtn: { height: 30, justifyContent: 'center', alignItems: 'flex-end' },
  timeCount: { height: 30, justifyContent: 'flex-end', alignItems: 'flex-end' },
  secondText: { color: 'red', fontSize: 18, textAlign: 'right' },
  iconsty: { color: '#ccc', fontSize: 16, },
  labelsty: { color: '#000' }


});


export default JoinMember;

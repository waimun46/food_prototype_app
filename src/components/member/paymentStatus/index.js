import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, } from 'react-native';
import { Text, Button } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";



class PaymentStatusScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const navigateActions = this.props.navigation.navigate;
    return (
      <View style={styles.container}>
        <ScrollView>

          <View style={styles.iconContainer}>
            <ANT name="checkcircle" size={100} color='#32CD32' />
          </View>
          <View style={{ padding: 20 }}>
            <View style={{ marginTop: 30, }}>
              <Text style={{ fontSize: RFPercentage(4.8), textAlign: 'center' }}>RM 200</Text>
              <Text style={{ marginTop: 10, textAlign: 'center' }}>Your Payment is Complete</Text>
            </View>

            <View style={{ marginTop: 30, }}>
              <Text note style={{ textAlign: 'center' }}>
                Congratulations , you have success to join our membership and earn 400 credit in your account.
              </Text>
            </View>

            <Button full dark style={styles.btnwarp} onPress={() => navigateActions('home')}>
              <Text style={styles.btntext}>BACK TO HOME</Text>
            </Button>
          </View>


        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: { alignItems: 'center', backgroundColor: '#fff', flex: 1, },
  iconWarp: { width: 130, height: 130, borderRadius: 100, alignItems: 'center', justifyContent: 'center' },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '100%', },
  btnwarp: { marginTop: 50, borderRadius: 5, height: 50, marginBottom: 50 },
  btntext: { color: '#fff', },
})

export default PaymentStatusScreen;
